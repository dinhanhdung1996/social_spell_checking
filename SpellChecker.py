import pickle
import Phrase


class SpellChecker:
    dict_1 = {}
    dict_2 = {}
    dict_3 = {}
    sum_dict1 = 0
    sum_dict2 = 0
    sum_dict3 = 0

    @staticmethod
    def read_dict(cache1=None, cache2=None, cache3=None):
        if cache1 is None and cache2 is None and cache3 is None:
            return
        SpellChecker.dict_1 = pickle.load(open(cache1, "rb"))
        SpellChecker.dict_2 = pickle.load(open(cache2, "rb"))
        SpellChecker.dict_3 = pickle.load(open(cache3, "rb"))
        SpellChecker.sum_dict1 = sum(SpellChecker.dict_1.values())
        SpellChecker.sum_dict2 = sum(SpellChecker.dict_2.values())
        SpellChecker.sum_dict3 = sum(SpellChecker.dict_3.values())

    def __init__(self):
        self.phrase = Phrase.Phrase()

    def get_string(self, string):
        self.phrase.get_string(string=string)

    def get_correct(self):
        temp_list = self.phrase.get_correct()
        m = temp_list.shape[0]
        temp_string = ""
        for i in range(m):
            temp_string += (temp_list[i] + " ")
        return temp_string

    def read_data(self,input_file="input.txt", output="output.txt"):
        file = open(input_file, "r")
        file_w = open(output, "w")
        for line in file.readlines():
            self.get_string(str(line))
            temp_string = self.get_correct()
            file_w.write(temp_string + "\n")
        file_w.close()
        file.close()