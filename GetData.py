from nltk import ngrams, FreqDist
import numpy
import pickle
import pymysql


class GetData:
    def __init__(self, cache1="cache1.p", cache2="cache2.p", cache3="cache3.p"):
        self.documents = []
        self.stream = []
        self.cache1 = cache1
        self.cache2 = cache2
        self.cache3 = cache3

    def connect_get_text(self, connection):
        cur = connection.cursor()
        cur.execute('SELECT content FROM news LIMIT 10000')
        rows = cur.fetchall()
        for text in rows:
            self.add_doc(str(text[0]))

    def read_db(self, host_name, user_name, pass_word, DB):
        my_connection = pymysql.connect(host=host_name, user=user_name, passwd=pass_word, db = DB,charset= 'utf8')
        self.connect_get_text(connection=my_connection)
        my_connection.close()

    def add_doc(self, document):
        if type(document) is not str:
            return
        if document in self.documents:
            return
        self.documents.append(document)
        temp = document.split()
        delete_list = []
        s = "\"'?_<>,./`~!@#$%^&*()-_=+:;"
        for i in range(len(list(temp))):
            temp[i] = temp[i].strip(s)
            temp[i] = temp[i].lower()
            for c in temp[i]:
                if c in s:
                    delete_list.append(i)
        for i in delete_list:
            temp[i] = ""
        self.stream += temp
        # with open("output.txt", "a") as file:
        #     file.write(document)

    def update_cache(self):
        all_counts = {}
        for size in range(1, 4):
            all_counts[size] = FreqDist(ngrams(self.stream, size))
            all_counts[size][""] = 0
            if size == 1:
                for a_word in list(all_counts[size].keys()):
                    if all_counts[size][a_word] < 5:
                        del all_counts[size][a_word]
                self.stream = list(x[0] for x in all_counts[size].keys())

        file = open(self.cache1, "wb")
        pickle.dump(all_counts[1], file)
        file = open(self.cache2, "wb")
        pickle.dump(all_counts[2], file)
        file = open(self.cache3, "wb")
        pickle.dump(all_counts[3], file)








