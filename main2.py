import SpellChecker
import os

my_folder = os.path.dirname(os.path.abspath(__file__))
cache1 = os.path.join(my_folder, "cache1.p")
cache2 = os.path.join(my_folder, "cache2.p")
cache3 = os.path.join(my_folder, "cache3.p")

SpellChecker.SpellChecker.read_dict(cache1, cache2, cache3)

def main():
    input_file = os.path.join(my_folder, "input.txt")
    output = os.path.join(my_folder, "output.txt")
    spell_checker = SpellChecker.SpellChecker()
    spell_checker.read_data(input_file=input_file, output=output)

main()


