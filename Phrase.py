import numpy
import SpellChecker
import editdistance
import math
from sklearn.utils.extmath import cartesian


class Phrase:
    def __init__(self):
        self.word_list = []
        self.new_phrases= numpy.array([])
        self.string = ""
        self.prob = []
        self.best_index = 0
        self.correct = numpy.array([])

    def get_string(self, string):
        if type(string) is not str:
            return
        self.string = string
        self.tokenization()

    def tokenization(self):
        self.word_list = self.string.split()
        temp_new_phrases = []
        for i in range(len(self.word_list)):
            self.word_list[i] = self.word_list[i].strip("!`~@#$%^&*()-_=+,<.>/?\\|\";:")
            self.word_list[i] = self.word_list[i].lower()
        for a_word in self.word_list:
            temp_phrase = numpy.array([])
            print(a_word)
            for word in SpellChecker.SpellChecker.dict_1:
                if editdistance.eval(a_word, word) <= 2.0:
                    temp_phrase = numpy.append(temp_phrase, numpy.array([word]))
                    print(a_word, " " ,word)
            if temp_phrase.shape[0] != 0:
                temp_new_phrases.append(temp_phrase)
        self.new_phrases = numpy.array(temp_new_phrases)
        self.new_phrases = self.cartesian()
        a=10

    def cartesian(self, result=None, next_index=0):
        n = numpy.prod([m.shape[0] for m in self.new_phrases[next_index:]if m.shape[0] != 0])
        data_type = self.new_phrases[next_index].dtype
        if result is None:
            result = numpy.zeros([n, self.new_phrases.shape[0]], dtype=data_type)
        m = int(n/self.new_phrases[next_index].shape[0])
        k = int(m * self.new_phrases[next_index].shape[0])
        result[0:k, next_index] = numpy.repeat(self.new_phrases[next_index], m)
        temp = next_index + 1
        if self.new_phrases[temp:].shape[0] > 0:
            self.cartesian(result=result, next_index=next_index+1)
            for j in range(1, self.new_phrases[next_index].shape[0]):
                result[j*m:(j+1)*m, next_index+1:] = result[0:m, temp:]
        return result

    @staticmethod
    def p_con(w2=None, w1=None):
        if w2 is None and w1 is None:
            return 0
        elif w2 is not None and w1 is None:
            return SpellChecker.SpellChecker.dict_1[w1]/SpellChecker.SpellChecker.sum_dict1
        freq_w1 = SpellChecker.SpellChecker.dict_1[w1]
        freq_w1_w2 = SpellChecker.SpellChecker.dict_2[(w1, w2)]
        return (freq_w1_w2+0.01)/(freq_w1 + 0.01 * SpellChecker.SpellChecker.sum_dict1)

    def prob_calculate(self, phrase):
        size = phrase.shape[0]
        prob = 0
        for i in range(size):
            if i == 0:
                Phrase.p_con(w2=phrase[i])
            prob += math.log(Phrase.p_con(w2=phrase[i], w1=phrase[i-1]))
        self.prob.append(prob)

    def prob_phrases(self):
        self.prob.clear()
        m = self.new_phrases.shape[0]
        for i in range(m):
            self.prob_calculate(self.new_phrases[i])
        best = 0
        best_value = self.prob[0]
        for i in range(len(self.prob)):
            if best_value > self.prob[i]:
                best = i
                best_value = self.prob[i]
        self.correct = self.new_phrases[best]

    def get_correct(self):
        self.prob_phrases()
        return self.correct

